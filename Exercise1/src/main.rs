// You should implement the following function:

fn sum_of_multiples(mut number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    let mut sum = 0;
    let mut num = multiple1;
    let mut i = 1;
    while num < number {
        sum += num;
        i += 1;
        num = multiple1 * i;
    }
    num = multiple2;
    i = 1;
    
    while num < number {
        sum += num;
        i += 1;
        num = multiple2 * i;
    }
    sum
}

fn main() {
    println!("{}", sum_of_multiples(10, 5, 3));
}
