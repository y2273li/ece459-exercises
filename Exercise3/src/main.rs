use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];
    for n in 0..N {
        let handle = thread::spawn(move || {
            println!("This is from thread {}", n);
        });
        children.push(handle);
    }
    for handle in children {
        handle.join().unwrap();
    }
}
